import xml.etree.ElementTree as ET
import sqlite3
import random
import time
import math
import numpy
import nltk
import operator
import pickle
from sklearn import datasets, linear_model
from nltk.stem.snowball import GermanStemmer



#### The following cells are for querying the database (take.db should be in the same folder as this notebook) 


def startdb():
    return sqlite3.connect('take.db')
    
def closedb(conn):
    conn.commit()
    conn.close()
    
def get_speech(eid, source='hand'):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from {} where episode_id = ? order by inc'.format(source),(eid,))
    result = [row for row in result] 
    closedb(conn)
    return result

def get_gaze(eid, source='hand', inc=0):
    conn = startdb()
    c = conn.cursor()
    if inc == 0:
        result = c.execute('select * from {}_gaze where episode_id = ? order by inc'.format(source), (eid,))
    else:
        result = c.execute('select * from {}_gaze where episode_id = ? and inc=?'.format(source), (eid,inc,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_deixis(eid, source='hand', inc=0):
    conn = startdb()
    c = conn.cursor()
    if inc == 0:
        result = c.execute('select * from {}_deixis where episode_id = ? order by inc'.format(source), (eid,))
    else:
        result = c.execute('select * from {}_deixis where episode_id = ? and inc=?'.format(source), (eid,inc,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_raw_data(eid):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from cv_piece_raw where episode_id = ?',(eid,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_selected_piece(eid):
    conn = startdb()
    c = conn.cursor()
    c.execute('select object from referent where episode_id = ?', (eid,))
    result = c.fetchone()[0]
    closedb(conn)
    return result


#### For extracting features from the database rows

def get_features(sample):
    '''
    A sample consists of raw information retrieved from the database including RGB, HSV, orientation values, as well
    as x,y coordiantes of the object's calcuated centroid, and how it has been skewed.
    
    '''
    features = {}
    f_name = 'f{}'
    f_ind = 1
    for feature in sample[2:-6]: # rgb, hsv, orientation
        features[f_name.format(str(f_ind))] = feature
        f_ind +=1 
    for feature in sample[-2:]: # x, y
        features[f_name.format(str(f_ind))] = feature
        f_ind +=1 
    features['num_edges'] = sample[11]
    if sample[9] == 'left-skewed': features['h_ls'] = 1.0
    else: features['h_ls'] = 0.0
    if sample[9] == 'right-skewed': features['h_rs'] = 1.0
    else: features['h_rs'] = 0.0
    if sample[9] == 'symmetric': features['h_s'] = 1.0
    else: features['h_s'] = 0.0   
        
    if sample[10] == 'top-skewed': features['v_ts'] = 1.0
    else: features['v_ts'] = 0.0
    if sample[10] == 'bottom-skewed': features['v_bs'] = 1.0
    else: features['v_bs'] = 0.0
    if sample[10] == 'symmetric': features['v_s'] = 1.0
    else: features['v_s'] = 0.0           
    return features


##### The follwing cells are for training and evaluation

def train(training_data):
    '''
    The training data is a list of dictionaries and binary class labels (True or False). The dictionary values are the raw values
    the features for the logreg classifier.
    '''
    x = []
    y = []
    for row in training_data:
        x.append(row[0].values())
        y.append(row[1])
        
    regr = linear_model.LogisticRegression()
    regr.fit(x, y)
    
    return regr


def is_training_data(j, i):
    '''
    returns True if an episode is within a fold's training set
    '''
    return (j < (i-1) * fold_size) or (j > i * fold_size)



# for training data, get the raw features from one sample and max_negs number of negative samples (5 works well)

def process_words(words, eid, utt, max_negs=5):
    '''
    This processes the words for TRAINING. It needs the words dictionary, the episode id, the utterance, and the number of 
    (randomly chosen) negative samples to use for training.
    '''
    selected = get_selected_piece(eid)
    raw_data = get_raw_data(eid)
    random.shuffle(raw_data)
    #grab the positive and negative samples from the set of 15 objects
    neg_samples = []
    pos_sample = None
    for r in raw_data:
        if r[1] == selected: pos_sample = r
        elif len(neg_samples) < max_negs: neg_samples.append(r)
        if len(neg_samples) == max_negs and pos_sample is not None: break
    if pos_sample is None: return
    pos_features = get_features(pos_sample)      
    for word in utt.strip().split():
        #word = stem.stem(word)
        if word not in words:
            words[word] = []
        words[word].append((pos_features,True))
        for neg_sample in neg_samples:
            neg_features = get_features(neg_sample)
            words[word].append((neg_features,False))    


def process_eval(eid):
    '''
    the process_words's counterpart for evaluation; this gathers the features for each object
    '''
    objects = {}
    raw_data = get_raw_data(eid)
    for row in raw_data:
        objects[row[1]] = get_features(row)      
    return objects

def argmax_tuple(dist):
    '''
    find the argmax of a distribution in a dictionary
    '''
    m = ('',0)
    for o in dist:
        if dist[o] > m[1]:
            m = (o,dist[o])
    return m

def argmax(dist):
    return argmax_tuple(dist)[0]

def calc_entropy(objects):
    entropy = 0.0
    for obj in objects:
        entropy += objects[obj] * math.log(objects[obj])
    return -entropy


def normalise(prob_dists, denom=0):
    '''
    Given a dictionary of object ids and probabilities, it normalises the dictionary and returns it 
    '''
    new_dists = {}
    k = denom
    if k == 0:
        k = sum(prob_dists.values())
    for obj in prob_dists:
        new_dists[obj] = prob_dists[obj] / k
    return new_dists


def classify(word, word_classifiers, objects):
    '''
    given a word, the word_classifiers, and the set of objects, this steps through each word in the utterance and each
    object and gets a probability distribution for each object given the results from the word classifiers
    '''
    prob_dists = {}
    if word in word_classifiers: #just ignore words we don't know anything about
        for obj in objects:
             prob_dist = word_classifiers[word].predict_proba(objects[obj].values())
             #prob_dist = word_classifiers[word].prob_classify(objects[obj])
             prob_dists[obj] = prob_dist[0][1]
             #prob_dists[obj] = prob_dist.prob(True)
    return normalise(prob_dists)


def euclidean_dist(x,y,objects):
    '''
    find the euclidean distance of all objects in a dictionary (x=f8, y=f9) and a point
    '''
    euclid_dists = []
    for o in objects:
        euclid_dists.append(math.sqrt((float(objects[o]['f8']) - float(x)) ** 2 + (float(objects[o]['f9']) - float(y)) ** 2))
    return euclid_dists


def point_calc(x1,x2,stddev):
    '''
    this will return a probability, given two points and a std dev ... the larger the std dev, the more mass is given when the
    two points are far away
    '''
    return math.exp(-pow((x1-x2),2.0) / (2.0 * math.pow(stddev,2.0))) 


def classify_modality(query_func, eid, inc, objects, source='hand'):
    '''
    This takes an episode id, then queries the db for the set of deixis points on the screen and their durations. For each point,
    if calculates the distance between that point and each object centroid, then determines a normalised probability distribution
    based on the distances. Then it awards the duration to each object based on its probability. This is summed for each deixis
    event, and normalised over for the total deixis duration. That resulting distribution is combined with the NLU distribution
    by additive interpolation.
    '''
    deixis = get_words(query_func(eid,source,inc))
    #for o in speech_dist: speech_dist[o] = 0.1
    if len(deixis) == 0: return {},0.0
    #if len(deixis) <= 1: return speech_dist
    if 'T' in deixis or 'B' in deixis: return {},0.0
    deixis = deixis.strip().split()
    
    total = 0.0
    all_object_probs = {}
    
    # step through each point
    for point in deixis:
        if ',' not in point: continue #this shouldn't happen, of course
        x,y,d = point.split(',')
        
        if str(d).lower() == 'nan': d = 1.0
        
        # calculate the std dev
        std = numpy.std(euclidean_dist(x, y, objects)) 
        if std == 0.0: std = 0.1
        
        object_probs = {}
        #determine the probabiliy for each object based on a normal distribution from the deixis point
        for obj in objects:
            if obj not in object_probs: object_probs[obj] = 0.0
            ox,oy = objects[obj]['f8'],objects[obj]['f9']
            prob = point_calc(float(ox),float(x),std) * point_calc(float(oy),float(y),std)
            object_probs[obj] = prob 
        object_probs = normalise(object_probs)
        
        # for the current point, award the duration to each object based on their probabilities
        for obj in object_probs:
            if obj not in all_object_probs: all_object_probs[obj] = 0.0
            all_object_probs[obj] += object_probs[obj] * float(d) 
        total += float(d)
        
    #for o in all_object_probs:
    #    all_object_probs[o] /= total

    return all_object_probs,total #normalise(all_object_probs)


##### Run the cross validation

# In[940]:

def interpolate(dists, weights):
    new_dist = {}
    for obj in dists[0]:
        row = 0.0
        i = 0
        for dist in dists:
            if obj in dist:
                row += dist[obj] * weights[i]
            i += 1
        new_dist[obj] = row
            
    return new_dist

def get_words(rows):
    return ' '.join([row[2] for row in rows]).strip()

def sum_dist(speech_dist, prob_dists):
    for obj in prob_dists:
        if obj not in speech_dist: speech_dist[obj] = 0.0
        speech_dist[obj] = speech_dist[obj] + prob_dists[obj]
        
        
def rank(dist, obj):
    sorted_obj = order_by_prob(dist)
    i = 1
    for o,p in sorted_obj:
        if o == obj:
            return i
        i += 1
    return 15

def order_by_prob(dist):
    sorted_obj = sorted(dist.items(), key=operator.itemgetter(1))
    sorted_obj.reverse()
    return sorted_obj

def calc_weights(speech, deixis, gaze, speech_model, deixis_model, gaze_model):
    
    d = 0.0
    g = 0.0
    s = 0.0
    
    if len(deixis) > 0:
        _,deixis = zip(*order_by_prob(deixis))
        d = deixis_model.predict_proba(deixis)[0][1]
    if len(gaze) > 0:
        _,gaze = zip(*order_by_prob(gaze))
        g = gaze_model.predict_proba(gaze)[0][1]
    if len(speech) > 0:
        _,speech = zip(*order_by_prob(speech))
        s = speech_model.predict_proba(speech)[0][1]        
        
    if s == 0: s= 0.5
    
    weights = [i / sum([s,d,g]) for i in [s,d,g]]
    #weights = [i / sum([s,g]) for i in [s,g]]
    #weights = [weights[0], 0.0, weights[1]] 
    return weights

'''
     Program begin
'''


source = 'hand'

conn = startdb()
c = conn.cursor()
res = c.execute('select distinct episode_id from {}'.format(source)) # order?
episodes = [row[0] for row in res]
closedb(conn)

stem = GermanStemmer()

fold_size = 100
num_folds = 10
max_eps = 1000
cor_tot = (0.0,0.0)
avg_rank = 0.0
entropy = 0.0

inc_results = open("/home/casey/Desktop/take_disc_hand_inc_results","w")

corr_list = []
all_object_dists = {}
all_objects = {}

# for n in range(1,6):
if True:
#     for i in range(1,2):      
    for i in range(1,num_folds+1):      
        print "processing fold {} out of {}".format(i, num_folds)
        
        #collect triaining data
        words_list = {}  # elements of type ({'feature_name':feature_value}:class_label)
        j = 1.0    
        k = 0
        deixis_data = []
        gaze_data = []
        speech_samples = []
        deixis_correct = []
        speech_correct= []
        gaze_correct = []
        for eid in episodes:
            utt = get_words(get_speech(eid,source))
            if j > max_eps: break
            if is_training_data(j, i):
                process_words(words_list, eid, utt)
                speech_samples.append(eid)
                gaze_total = 0.0
                deixis_total = 0.0  
                gaze_dist = {}
                deixis_dist = {}   
                selected = get_selected_piece(eid)
                objects = process_eval(eid)
                
                for _,inc,word,_,_ in get_speech(eid,source):
                    prob_deixis,t = classify_modality(get_deixis, eid, inc, objects, source)
                    deixis_total += t
                    if len(prob_deixis) != 0: 
                        sum_dist(deixis_dist, prob_deixis)
                    
                    prob_gaze,t = classify_modality(get_gaze, eid, inc, objects, source)
                    gaze_total += t
                    if len(prob_gaze) != 0: 
                        sum_dist(gaze_dist, prob_gaze)
                        
                d = normalise(deixis_dist, deixis_total)
                if len(d) == 15:
                    deixis_correct.append(str(argmax(d) == selected))   
                    _,d = zip(*order_by_prob(d))
                    deixis_data.append(d)
                
                g = normalise(gaze_dist, gaze_total)
                if len(g) == 15:
                    gaze_correct.append(str(argmax(g) == selected))
                    _,g = zip(*order_by_prob(g))
                    gaze_data.append(g) 
                                   
            j += 1.0
        
        #train
        word_classifiers = {}
        for word in words_list:
            word_classifiers[word] = train(words_list[word]) 
        
        speech_data = []
        for eid in speech_samples:
            selected = get_selected_piece(eid)
            objects = process_eval(eid)
            speech_dist = {}
            for _,inc,word,_,_ in get_speech(eid,source):
                prob_speech = classify(word, word_classifiers, objects)
                if len(prob_speech) != 0 :  # sometimes we don't know the word because it wasn't in training
                    sum_dist(speech_dist, prob_speech)
            s = normalise(speech_dist)
            if len(s) == 15:
                speech_correct.append(str(argmax(s) == selected))   
                _,s = zip(*order_by_prob(s))
                speech_data.append(s)
                
        regr = linear_model.LogisticRegression()
        deixis_relevance = regr.fit(deixis_data, deixis_correct) 
        regr = linear_model.LogisticRegression()
        gaze_relevance = regr.fit(gaze_data, gaze_correct)
        regr = linear_model.LogisticRegression()
        speech_relevance = regr.fit(speech_data, speech_correct) 
            
        #evaluate    
        j = 1.0
        for eid in episodes:
            utt = get_words(get_speech(eid,source))
            #collect triaining data for the fold
            if j > max_eps: break
            if not is_training_data(j, i):
                selected = get_selected_piece(eid)
                objects = process_eval(eid)
                
                gaze_total = 0.0
                deixis_total = 0.0  
                   
                speech_dist = {}
                gaze_dist = {}
                deixis_dist = {}                

                #step through each word
                final_dist = {}
                for _,inc,word,_,_ in get_speech(eid,source):
                    
                    prob_speech = classify(word, word_classifiers, objects)
                    if len(prob_speech) == 15 :  # sometimes we don't know the word because it wasn't in training
                        sum_dist(speech_dist, prob_speech)
                         
                    prob_deixis,t = classify_modality(get_deixis, eid, inc, objects, source)
                    deixis_total += t
                    if len(prob_deixis) != 0: 
                        sum_dist(deixis_dist, prob_deixis)
    
                    prob_gaze,t = classify_modality(get_gaze, eid, inc, objects, source)
                    gaze_total += t
                    if len(prob_gaze) != 0: 
                        sum_dist(gaze_dist, prob_gaze)
                        
                    #weights = calc_weights(normalise(speech_dist), normalise(deixis_dist, deixis_total), normalise(gaze_dist, gaze_total), speech_relevance, deixis_relevance, gaze_relevance)
                    weights = [1.0, 0.0, 0.0]
                    final_dist = interpolate([normalise(speech_dist), normalise(deixis_dist, deixis_total), normalise(gaze_dist, gaze_total)], weights)
    
                    final_dist = normalise(final_dist)
                    guess = argmax(final_dist)
                    entropy += calc_entropy(final_dist)
                    avg_rank += rank(final_dist, selected)
                
                    gotit = "False"
                    if guess == selected:
                        cor_tot = (cor_tot[0] + 1.0, cor_tot[1])
                        corr_list.append(eid)
                        gotit = "True"
                    inc_results.write(gotit + " ")
                        
                cor_tot = (cor_tot[0], cor_tot[1]+1.0)
                inc_results.write("\n")
            j += 1.0
    
        print "acc", cor_tot, cor_tot[0]/cor_tot[1]
        print "avg rank", avg_rank / cor_tot[1]
        print "entropy:", entropy / cor_tot[1]
inc_results.close()

