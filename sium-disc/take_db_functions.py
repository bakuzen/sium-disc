'''
Created on Oct 24, 2014

@author: casey
'''

import math

# grid + row + col = (y,x)
centroids = {}
centroids['grid1'] = {}
centroids['grid2'] = {}
centroids['grid3'] = {}
centroids['grid4'] = {}
for grid in centroids:
    centroids[grid] = {}
    for i in range(0,3):
        centroids[grid][i] = {}


grid = 'grid1'
centroids[grid][0][0] = (175,125)
centroids[grid][0][1] = (175,230)
centroids[grid][0][2] = (175,355)
centroids[grid][1][0] = (295,125)
centroids[grid][1][1] = (295,230)
centroids[grid][1][2] = (295,355)
centroids[grid][2][0] = (400,125)
centroids[grid][2][1] = (400,230)
centroids[grid][2][2] = (400,355)

grid = 'grid2'
centroids[grid][0][0] = (1495,125)
centroids[grid][0][1] = (1495,230)
centroids[grid][0][2] = (1495,355)
centroids[grid][1][0] = (1595,125)
centroids[grid][1][1] = (1595,230)
centroids[grid][1][2] = (1595,355)
centroids[grid][2][0] = (1715,125)
centroids[grid][2][1] = (1715,230)
centroids[grid][2][2] = (1715,355)

grid = 'grid3'
centroids[grid][0][0] = (175,700)
centroids[grid][0][1] = (175,805)
centroids[grid][0][2] = (175,915)
centroids[grid][1][0] = (295,700)
centroids[grid][1][1] = (295,805)
centroids[grid][1][2] = (295,915)
centroids[grid][2][0] = (400,700)
centroids[grid][2][1] = (400,805)
centroids[grid][2][2] = (400,915)


grid = 'grid4'
centroids[grid][0][0] = (1495,700)
centroids[grid][0][1] = (1495,805)
centroids[grid][0][2] = (1495,915)
centroids[grid][1][0] = (1595,700)
centroids[grid][1][1] = (1595,805)
centroids[grid][1][2] = (1595,915)
centroids[grid][2][0] = (1715,700)
centroids[grid][2][1] = (1715,805)
centroids[grid][2][2] = (1715,915)

def find_id(x, y, pieces):
    global centroids
    #print x, y
    closest_dist = 9999999999.0
    closest_tile = None
    #print
    for piece in pieces:
        grid = piece[3]
        col = int(piece[4])
        row = int(piece[5])
        #NOTE!!! this should have xi, yi but they were backwards in the first round of parser output
        yi, xi = centroids[grid][col][row]
        dist = math.sqrt((xi-x)**2+(yi-y)**2)
        #print x, y, dist, piece[0]
        if dist < closest_dist:
            closest_dist = dist
            closest_tile = piece[0]
        
    return closest_tile