'''
Created on 26.01.2015

@author: ckennington
'''
import sqlite3
import random
import nltk

conn = sqlite3.connect('takecv.db')
    
c = conn.cursor()
result = c.execute('select * from take_tags order by episode_id, inc')
result = [row for row in result] 

conn.commit()
conn.close()

def get_features(ids, data):
    train = list()
    
    for i in ids:
        prev = ('<s>', '<s>', '<t>', '<t>')
        for word, tag in data[i]:
            features = {'word':word, 'p_word': prev[0], 'p_word':prev[1], 'pp_tag':prev[2], 'p_tag':prev[3]}
            train.append((features,tag))
            prev = (features['p_word'], features['word'], features['p_tag'], tag)
    return train

data = {}
for row in result:
    if row[0] not in data: data[row[0]] = list()
    data[row[0]].append(row[2:])

ids = data.keys()
random.shuffle(ids)

train_ids = ids[:945]
eval_ids = ids[945:]
train = get_features(train_ids, data)
eval = get_features(eval_ids, data)

classifier = nltk.NaiveBayesClassifier.train(train)

print(nltk.classify.accuracy(classifier, eval))

    
    
    