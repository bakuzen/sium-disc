import xml.etree.ElementTree as ET
import sqlite3
import pandas as pd
import os
from mumodoIO import *
from analysis import *
import take_db_functions

try:
    os.remove("take.db")
except OSError:
    pass

corpus_path = '/home/casey/git/001_sinlu14/001_sinlu14/evaluation/'
transc_path = '/home/casey/git/004_take-corpus-transcriptions/{0}/{1}/{2}'
scene_path = '/home/casey/Desktop/TAKE/{0}/{0}/{1}/{2}'

print "extracting data from files..."

# the episode_data dict will hold everything for us to add to the db
episode_data = {}

hand = [line.strip().split('|') for line in open(corpus_path+"trans")]
asr  = [line.strip().split('|') for line in open(corpus_path+"reco")]

# get the referrred object's id
for line in hand: episode_data[line[0]] = [line[2]]

# get the hand transc and asr
for source,filename in [(hand,'adjusted_reco.TextGrid'), (asr,'google_asr_44k.TextGrid')]:
    for line in source:
        p,i = line[0].split('.')
        if line[0] not in episode_data: episode_data[line[0]] = []
        try:
            episode_data[line[0]].append(open_intervalframe_from_textgrid(transc_path.format(p,i,filename))['Utterances'])
        except IOError:
            episode_data[line[0]].append(pd.DataFrame())

for eid in episode_data:
    for speech in episode_data[eid][1:3]:
        prev_row = None
        for row in speech.iterrows():
            if prev_row is not None:
                speech['start_time'][row[0]] = prev_row[1]['end_time']
            prev_row = row

# print episode_data['r6.72'][2]
#for line in hand: episode_data[line[0]].append(line[5])
#for line in asr: episode_data[line[0]].append(line[5])

# get the scene information from the xml files
for line in hand:
    try:
        if line[0] not in episode_data: continue
        p,i = line[0].split('.')
        xml_path = scene_path.format(p, i ,line[1])
        tree = ET.parse(xml_path)
        objects = tree.getroot().find("objects")
        pieces = []
        for piece in objects.findall('piece'):
            p = [piece.attrib['id'], piece.attrib['color'], piece.attrib['type']] 
            sfs = piece.find("start-field")
            grid,xy = sfs.text.split(".")
            row,col = xy.split("-")
            p += [grid, row, col]
            p.append(take_db_functions.centroids[grid][int(row)][int(col)])
            pieces.append(p)
        episode_data[line[0]].append(pieces)
    except IOError: del episode_data[line[0]]

print "getting deixis data"
# get the deixis data
for eid in episode_data:
    p,i = eid.split('.')
    try: 
        left = open_intervalframe_from_textgrid(transc_path.format(p,i,'deixis_interps_left_hand_shoulder_intersect.TextGrid'))['deixis']
    except IOError: left = pd.DataFrame()
    try:
        right = open_intervalframe_from_textgrid(transc_path.format(p,i,'deixis_interps_right_hand_shoulder_intersect.TextGrid'))['deixis']
    except IOError: right = pd.DataFrame()
            
    deixis = left.append(right)
    if deixis.empty: continue
    
    deixis.index = deixis['start_time']
    deixis = deixis.sort_index()
    
    for speech in episode_data[eid][1:3]: # align with hand AND asr
        if speech.empty: continue
        try:
            speech['start_time'][0] = 0.0
        except TypeError, KeyError:
            print speech
        speech['deixis'] = [deixis.ix[row[1]['start_time']:row[1]['end_time']] for row in speech.iterrows()]
    

# get the gaze data
print "getting gaze data"
for eid in episode_data:
    p,i = eid.split('.')
    try: 
        gaze = open_intervalframe_from_textgrid(transc_path.format(p,i,'gaze.TextGrid'))['gaze']
    except IOError: gaze = pd.DataFrame()
    if gaze.empty: continue
    
    gaze.index = gaze['start_time']
    gaze = gaze.sort_index()
    
    for speech in episode_data[eid][1:3]: # align with hand AND asr
        if speech.empty: continue
        try:
            speech['start_time'][0] = 0.0
        except TypeError:
            print speech
        speech['gaze'] = [gaze.ix[row[1]['start_time']:row[1]['end_time']] for row in speech.iterrows()]
    

#print episode_data['r4.114']

#create the tables

print "creating tables..."

conn = sqlite3.connect('take.db')

c = conn.cursor()

c.execute('''CREATE TABLE hand
             (episode_id text, inc integer, word text, start_time real, end_time real)''')

c.execute('''CREATE TABLE hand_gaze
             (episode_id text, inc integer, gaze text)''')

c.execute('''CREATE TABLE hand_deixis
             (episode_id text, inc integer, deixis text)''')

c.execute('''CREATE TABLE asr
             (episode_id text, inc integer, word text, start_time real, end_time real)''')

c.execute('''CREATE TABLE asr_gaze
             (episode_id text, inc integer, gaze text)''')

c.execute('''CREATE TABLE asr_deixis
             (episode_id text, inc integer, deixis text)''')

c.execute('''CREATE TABLE referent
             (episode_id text, object text)''')

c.execute('''CREATE TABLE piece
             (episode_id text, id text, color text, type text, grid text, row integer, col integer, x integer, y integer)''')

c.execute('''CREATE TABLE cv_piece
             (episode_id text, id text, color text, top_color text, type text, top_type text, grid text)''')

c.execute('''CREATE TABLE cv_piece_raw
             (episode_id text, id text, r double, g double, b double, h double, s double, v double, orientation double,
             h_skew text, v_skew text, num_edges integer, position text, pos_x integer, pos_y integer)''')

conn.commit()
conn.close()


# episode_data consists of a dictionary of lists with [ref_id, hand, asr, pieces]

# now insert the data

print "inserting speech, gaze, deixis..."

conn = sqlite3.connect('take.db')
c = conn.cursor()

#TODO: there might be gaps, for example between the last word and the end of the episode that aren't considered here

#insert the speech, deixis, gaze
for eid in episode_data:
    for speech,source in [(episode_data[eid][1],'hand'),(episode_data[eid][2],'asr')]:
        inc = 1 
        for row in speech.iterrows():
            #insert the speech
            text = row[1]['text']
            start_time = row[1]['start_time']
            end_time = row[1]['end_time']
            c.execute("INSERT INTO {0} VALUES (?,?,?,?,?)".format(source),(eid, inc, text,start_time,end_time))
            #accumulate and insert the deixis
            try:
                text = ' '.join([d_row[1]['text'].encode('utf-8')+',1' for d_row in row[1]['deixis'].iterrows()]).strip()
                c.execute("INSERT INTO {0}_deixis VALUES (?,?,?)".format(source),(eid, str(inc), text))
            except KeyError: pass
            #accumulate and insert the gaze
            try:
                text = ' '.join([d_row[1]['text'].encode('utf-8')+',1' for d_row in row[1]['gaze'].iterrows()]).strip()
                c.execute("INSERT INTO {0}_gaze VALUES (?,?,?)".format(source),(eid, str(inc), text))
            except KeyError: pass
            inc += 1

# insert the pieces

print "inserting referent, pieces..."
 
for eid in episode_data:
    c.execute("INSERT INTO REFERENT VALUES (?,?)",(eid, episode_data[eid][0]))
    for piece in episode_data[eid][3]:
        if type(piece) is not list: continue
        c.execute("INSERT INTO piece VALUES (?,?,?,?,?,?,?,?,?)", (eid, piece[0], piece[1], piece[2], piece[3], piece[4], piece[5], piece[6][0], piece[6][1]))
        
conn.commit()
conn.close()


# the rest of this code was copied from an earlier notebook and is uuuuuggggllllyyy because it parses an xml file, but it works fairly robustly

print "inserting properties from predicted cv..."

# first, insert the computer vision data which has the predicted properties for SIUM
path = "/home/casey/Desktop/modified_TAKE_images/"

conn = sqlite3.connect('take.db')
c = conn.cursor()

lines = [line.strip() for line in open(corpus_path+'trans')]

for line in lines:
    line = line.split('|')
    e = line[0]
    p = line[0].split('.')[0]
    i = line[0].split('.')[1]
    xml_path = path +  p + '/' + i + '/' + line[1] 
    tree = ET.parse(xml_path)
    root = tree.getroot()
    if line[0] not in episode_data: continue
    #if type(episode_data[line[0]][0]) is not list: continue
    l = len(root.findall("object"))
    if l < 15: 
        #hack for the moment, if an xml file does not have 15 objects, use the other one in the folder
        new_xml_path = path +  p + '/' + str(i) + '/'
        for f in os.listdir(new_xml_path):
            if '.xml' in f and f != line[1]:
                xml_path = path +  p + '/' + str(i) + '/' + f
                tree = ET.parse(xml_path)
                root = tree.getroot()
    l = len(root.findall("object"))
    if l < 15: print e
    ids = ""
    for o in root.findall("object"):
        pos = o.find('position')
        _id = take_db_functions.find_id(int(pos.attrib['y']), int(pos.attrib['x']), episode_data[line[0]][3])
        ids += o.attrib['id']+','
        shape = o.find('shape')
        shape_dist = shape.find('distribution')
        shapes = ''
        for attr in shape_dist.attrib:
            shapes += attr.lower() + ':' + shape_dist.attrib[attr] + ','
        shapes = shapes[:-1]
        color = o.find('colour')
        color_dist = color.find('distribution')
        colors = ''
        for attr in color_dist.attrib:
            colors += attr.lower() + ':' + color_dist.attrib[attr] + ','
        colors = colors[:-1]
        
        c.execute("INSERT INTO cv_piece VALUES (?,?,?,?,?,?,?)", (e, _id, colors, color.attrib['BestResponse'].lower(),
                                                              shapes, shape.attrib['BestResponse'].lower(), pos.attrib['global'].replace(' ','-')))

    
# then, insert the raw values (they are also in the xml files)

print "inserting raw cv data..."
    
for line in lines:
    line = line.split('|')
    e = line[0]

    p = line[0].split('.')[0]
    i = line[0].split('.')[1]
    xml_path = path +  p + '/' + i + '/' + line[1] 

    tree = ET.parse(xml_path)
    root = tree.getroot()
    if line[0] not in episode_data: continue
    #if type(episode_data[line[0]][0]) is not list: continue
        
    l = len(root.findall("object"))
    if l < 15: 
        #hack for the moment, if an xml file does not have 15 objects, use the other one in the folder
        new_xml_path = path +  p + '/' + str(i) + '/'
        for f in os.listdir(new_xml_path):
            if '.xml' in f and f != line[1]:
                xml_path = path +  p + '/' + str(i) + '/' + f
                tree = ET.parse(xml_path)
                root = tree.getroot()        

    for o in root.findall("object"):
        pos = o.find('position')
        _id = take_db_functions.find_id(int(pos.attrib['y']), int(pos.attrib['x']), episode_data[line[0]][3])
        ids += o.attrib['id']+','
        color = o.find('colour')
        hsv = color.find('hsvValue')
        rgb = color.find('rgbValue')
        if rgb is None: rgb = color.findall('hsvValue')[1] 
        shape = o.find('shape')
        orientation = shape.find('orientation').attrib['value']
        skewness = shape.find('skewness')
        num_edges = shape.find('nbEdges').attrib['value']
        c.execute("INSERT INTO cv_piece_raw VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
                  (e, _id, rgb.attrib['R'], rgb.attrib['G'], rgb.attrib['B'],hsv.attrib['H'],hsv.attrib['S'],hsv.attrib['V'],
                   orientation, skewness.attrib['horizontal'], skewness.attrib['vertical'],
                   num_edges,pos.attrib['global'], pos.attrib['x'], pos.attrib['y']))    
        
conn.commit()
conn.close()

print "done!"