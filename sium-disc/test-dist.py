'''
Created on Jan 9, 2015

@author: casey
'''

from sklearn import datasets, linear_model

def get_positives_negatives(data):
    good = [line[2][1:-1].split("][") for line in data if line[1] == '1']
    good = [[float(item.split(':')[1]) for item in line] for line in good]
    bad = [line[2][1:-1].split("][") for line in data if line[1] != '1']
    bad = [[float(item.split(':')[1]) for item in line] for line in bad]
    return good, bad    

train = open('dist-train').readlines()
eval = open('dist-eval').readlines()

train = [line.split() for line in train]
eval = [line.split() for line in eval]

#train
pos, neg = get_positives_negatives(train)

x = pos + neg
y = [True for p in pos] + [False for p in neg]  

regr = linear_model.LogisticRegression()
regr.fit(x, y)

# evaluate
pos, neg = get_positives_negatives(eval)
print pos
print neg

pos = [regr.predict_proba(p)[0][1] for p in pos]
neg = [regr.predict_proba(p)[0][1] for p in neg]

print pos
print neg
total = len(eval)



