'''
Created on Dec 10, 2014

@author: casey
'''
import os
from increco import interval_frame_from_reco
from increco import IncReco
from mumodo.mumodoIO import *


path = '/home/casey/git/004_take_further_analysis/ASR/Google/44KHz/original/'
target = '/home/casey/git/004_take-corpus-transcriptions/'

for i in range(1,8):
    in_path =  '{}r{}/'.format(path, str(i))
    files = os.listdir(in_path)
    for f in files:

        rid = f.split('.')[1]
        reco = IncReco(in_path + f) 
        frame = interval_frame_from_reco(reco, lastonly=True)
        try:
            #save_intervalframe_to_textgrid({'Utterances':frame}, '{}r{}/{}/'.format(target, str(i), rid) + "google_asr_44k.TextGrid")
            save_intervalframe_to_textgrid({"utterance":frame}, "fake.TextGrid")
        except ValueError:
            print f
            print frame
        except IOError:
            print f
        
