import pandas as pd

class IncReco():
    def __init__(self, filepath):
        self.filepath = filepath
        self.inc_chunks = []
        self.parse()

    def __str__(self):
        return str(map(lambda x: str(x), self.inc_chunks))

    def parse(self):
        lines = open(self.filepath).read().splitlines()
        cur = []
        last_end = -1
        prev = ['','','']
        for l in lines:
            if l == "":
                if prev[2] != "":
                    cur.append(prev)
                self.inc_chunks.append(cur)
                cur = []
                prev = ['','','']
                continue
            l = l.split("\t")

            if len(l) <= 2 : continue

            if prev[1] != "" and float(l[1]) == float(prev[1]):
                prev[2] = prev[2]+" "+l[2]
                continue

            if prev[2] != "":
                cur.append(prev)
            prev = l

    def get_last(self):
        return self.inc_chunks[-1]

def interval_frame_from_reco(reco, lastonly=False):
    """ Create an interval frame from an inc_reco file

        Arguments:

        reco - an Inc_Reco object

        Kwargs:

        lastonly -- Read only the last chunk in the inc_reco file
                    rather than all chunks
    """

    start_chunk = -1 if lastonly else 0

    frame = None
    for utt in reco.inc_chunks[start_chunk:]:
        reco_frame = pd.DataFrame({"start_time": map(lambda x: \
                                           float(x[0].decode("utf-8")), utt),
                                   "end_time": map(lambda x: \
                                           float(x[1].decode("utf-8")), utt),
                                   "text": map(lambda x: \
                                           x[2].decode("utf-8"), utt)})
        if frame is None:
            frame = reco_frame
        else:
            frame = frame.append(reco_frame, ignore_index=True)

    #check for minor errors in the times
    for i in frame.index[1:]:
        if frame['start_time'].ix[i] < frame['end_time'].ix[i-1]:
            frame['start_time'].ix[i] = frame['end_time'].ix[i-1]

    return frame.ix[:, ['start_time', 'end_time', 'text']]
