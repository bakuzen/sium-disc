import sqlite3
import random
import math
import colorsys
import operator
from sklearn import linear_model
import matplotlib.pyplot as plt
import numpy as np
from nltk.stem.snowball import GermanStemmer
#### The following cells are for querying the database (take.db should be in the same folder as this notebook) 


def startdb():
    return sqlite3.connect('takecv.db')
    
def closedb(conn):
    conn.commit()
    conn.close()
    
def get_speech(eid, source='hand'):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from {} where episode_id = ? order by inc'.format(source),(eid,))
    result = [row for row in result] 
    closedb(conn)
    return result

def get_gaze(eid, source='hand', inc=0):
    conn = startdb()
    c = conn.cursor()
    if inc == 0:
        result = c.execute('select * from {}_gaze where episode_id = ? order by inc'.format(source), (eid,))
    else:
        result = c.execute('select * from {}_gaze where episode_id = ? and inc=?'.format(source), (eid,inc,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_deixis(eid, source='hand', inc=0):
    conn = startdb()
    c = conn.cursor()
    if inc == 0:
        result = c.execute('select * from {}_deixis where episode_id = ? order by inc'.format(source), (eid,))
    else:
        result = c.execute('select * from {}_deixis where episode_id = ? and inc=?'.format(source), (eid,inc,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_raw_data(eid):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from cv_piece_raw where episode_id = ?',(eid,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_selected_piece(eid):
    conn = startdb()
    c = conn.cursor()
    c.execute('select object from referent where episode_id = ?', (eid,))
    result = c.fetchone()[0]
    closedb(conn)
    return result

def get_landmark_piece(eid):
    conn = startdb()
    c = conn.cursor()
    c.execute('select object from landmark where episode_id = ?', (eid,))
    result = c.fetchone()
    closedb(conn)
    if result is None: return None # there isn't always a known landmark
    return result[0]


#### For extracting features from the database rows

def get_features(sample):
    '''
    A sample consists of raw information retrieved from the database including RGB, HSV, orientation values, as well
    as x,y coordiantes of the object's calcuated centroid, and how it has been skewed.
    
    '''
    features = {}
    f_name = 'f{}'
    f_ind = 1
    for feature in sample[2:-6]: # rgb, hsv, orientation
        features[f_name.format(str(f_ind))] = feature
        f_ind +=1 
    for feature in sample[-2:]: # x, y
        features[f_name.format(str(f_ind))] = feature
        f_ind +=1 
    features['num_edges'] = sample[11]
    if sample[9] == 'left-skewed': features['h_ls'] = 1.0
    else: features['h_ls'] = 0.0
    if sample[9] == 'right-skewed': features['h_rs'] = 1.0
    else: features['h_rs'] = 0.0
    if sample[9] == 'symmetric': features['h_s'] = 1.0
    else: features['h_s'] = 0.0   
        
    if sample[10] == 'top-skewed': features['v_ts'] = 1.0
    else: features['v_ts'] = 0.0
    if sample[10] == 'bottom-skewed': features['v_bs'] = 1.0
    else: features['v_bs'] = 0.0
    if sample[10] == 'symmetric': features['v_s'] = 1.0
    else: features['v_s'] = 0.0           
    return features


##### The follwing cells are for training and evaluation

def train(training_data):
    '''
    The training data is a list of dictionaries and binary class labels (True or False). The dictionary values are the raw values
    the features for the logreg classifier.
    '''
    x = []
    y = []
    for row in training_data:
        x.append(row[0].values())
        y.append(row[1])
    regr = linear_model.LogisticRegression(penalty='l1')
    regr.fit(x, y)
    
    return regr


def is_training_data(j, i):
    '''
    returns True if an episode is within a fold's training set
    '''
    return (j < (i-1) * fold_size) or (j > i * fold_size)



# for training data, get the raw features from one sample and max_negs number of negative samples 
def process_words(words, eid, utt, selected, max_negs=3):
    '''
    This processes the words for TRAINING. It needs the words dictionary, the episode id, the utterance, and the number of 
    (randomly chosen) negative samples to use for training.
    '''
    global stemmer

    raw_data = get_raw_data(eid)
    random.shuffle(raw_data)
    #grab the positive and negative samples from the set of 15 objects
    neg_samples = []
    pos_sample = None
    for r in raw_data:
        if r[1] == selected: pos_sample = r
        elif len(neg_samples) < max_negs: neg_samples.append(r)
        if len(neg_samples) == max_negs and pos_sample is not None: break
    if pos_sample is None: return
    pos_features = get_features(pos_sample)      
    for _,_,word,_,_,tags,correct in utt:
        if not check_tags(tags): continue
        word = stemmer.stem(word)
        #if not correct: continue
        if word not in words:
            words[word] = []
        words[word].append((pos_features,True))
        for neg_sample in neg_samples:
            neg_features = get_features(neg_sample)
            words[word].append((neg_features,False))    


def process_eval(eid):
    '''
    the process_words's counterpart for evaluation; this gathers the features for each object
    '''
    objects = {}
    raw_data = get_raw_data(eid)
    for row in raw_data:
        objects[row[1]] = get_features(row)      
    return objects

def argmax_tuple(dist):
    '''
    find the argmax of a distribution in a dictionary
    '''
    m = ('',0)
    for o in dist:
        if dist[o] > m[1]:
            m = (o,dist[o])
    return m

def argmax(dist):
    return argmax_tuple(dist)[0]

def calc_entropy(objects):
    entropy = 0.0
    for obj in objects:
        entropy += objects[obj] * math.log(objects[obj])
    return -entropy


def normalise(prob_dists, denom=0):
    '''
    Given a dictionary of object ids and probabilities, it normalises the dictionary and returns it 
    '''
    new_dists = {}
    k = denom
    if k == 0:
        k = sum(prob_dists.values())
    for obj in prob_dists:
        new_dists[obj] = prob_dists[obj] / k
    return new_dists


def classify(word, word_classifiers, objects):
    '''
    given a word, the word_classifiers, and the set of objects, this steps through each word in the utterance and each
    object and gets a probability distribution for each object given the results from the word classifiers
    '''
    prob_dists = {}
    if word in word_classifiers: #just ignore words we don't know anything about
        for obj in objects:
            prob_dist = word_classifiers[word].predict_proba(objects[obj].values())
            #prob_dist = word_classifiers[word].prob_classify(objects[obj])
            prob_dists[obj] = prob_dist[0][1]
            #prob_dists[obj] = prob_dist.prob(True)
    return normalise(prob_dists)


def euclidean_dist(x,y,objects):
    '''
    find the euclidean distance of all objects in a dictionary (x=f8, y=f9) and a point
    '''
    euclid_dists = []
    for o in objects:
        euclid_dists.append(math.sqrt((float(objects[o]['f8']) - float(x)) ** 2 + (float(objects[o]['f9']) - float(y)) ** 2))
    return euclid_dists


def point_calc(x1,x2,stddev):
    '''
    this will return a probability, given two points and a std dev ... the larger the std dev, the more mass is given when the
    two points are far away
    '''
    return math.exp(-pow((x1-x2),2.0) / (2.0 * math.pow(stddev,2.0))) 

def sum_dist(speech_dist, prob_dists):
    for obj in prob_dists:
        if obj not in speech_dist: speech_dist[obj] = 0.0
        speech_dist[obj] = speech_dist[obj] + prob_dists[obj]
        
        
def rank(dist, obj):
    sorted_obj = order_by_prob(dist)
    i = 1
    for o,_ in sorted_obj:
        if o == obj:
            return i
        i += 1
    return len(dist)

def order_by_prob(dist):
    sorted_obj = sorted(dist.items(), key=operator.itemgetter(1))
    sorted_obj.reverse()
    return sorted_obj

def incremental_metrics(guess, selected, ct, new=False):
    inc_results = open('','a')
    if new : inc_results.write('\n')
    gotit = "False"
    if guess == selected:
        gotit = "True"
    inc_results.write(gotit + " ")
    
def check_tags(tags):
    #if 'o' in tags: return False
    #if 'x' in tags: return False
    if 'tdc' in tags: return True
    if 'tds' in tags: return True
    if 'tdf' in tags: return True
    
    
    return False
    
    
def check_select_words(word_classifiers):
    global select_words
    spectrum = plt.get_cmap('spectral')
    objects = process_eval('r5.30.p1')
    dummy = objects['0'].values()
    gradient = np.linspace(0, 1, 256)
    gradient = np.vstack((gradient, gradient))
    word_responses = {}
    for word in select_words:
        responses = []
        for i in gradient[0]:
            rgb_coord = spectrum(i)[:-1]
            hsv_coord = colorsys.rgb_to_hsv(*list(rgb_coord))
            rgb = np.array(rgb_coord) * 255
            hsv0 = hsv_coord[0] * 180
            hsv = np.array(hsv_coord) * 255
            hsv[0] = hsv0
            this_object = list(rgb) + list(hsv) + dummy[6:]
            responses.append(word_classifiers[word].predict_proba(this_object)[0][1])
        word_responses[word] = responses
    for i, word in enumerate(select_words):
        plt.figure(i)
        plt.subplot(211)
        plt.plot(gradient[0], word_responses[word])
        plt.title(word)
        plt.subplot(212)
        plt.imshow(gradient, aspect='auto', cmap=spectrum)
    plt.show()


def prepare_training(eid, words_list, sourc='hand'):
    utt = get_speech(eid,source)
    target = get_selected_piece(eid)
    process_words(words_list, eid, utt, target)
    
def perform_eval(eid,word_classifiers, source='hand'):
    global stemmer
    
    selected = get_selected_piece(eid)
    objects = process_eval(eid)
    
    speech_dist = {}
    for _,_,word,_,_,tags,correct in get_speech(eid,source):
        if not check_tags(tags): continue
        #if not correct: continue
        word = stemmer.stem(word)
        prob_speech = classify(word, word_classifiers, objects)
        sum_dist(speech_dist, prob_speech)
    final_dist = normalise(speech_dist)
    return rank(final_dist, selected)
    
'''
     Program begin
'''


source = 'asr'

stemmer = GermanStemmer()

select_words = ['rot','blau','grau','pink','kreuz']

conn = startdb()
c = conn.cursor()
res = c.execute("select distinct episode_id from {} where episode_id like '%p1' order by episode_id, inc".format(source)) # order?
#res = c.execute("select distinct episode_id from {} order by episode_id, inc".format(source)) # order?
episodes = [row[0] for row in res]
closedb(conn)

fold_size = 43
num_folds = 10
max_eps = fold_size * num_folds
cor_tot = (0.0,0.0)
avg_rank = 0.0
entropy = 0.0

corr_list = []
all_object_dists = {}
all_objects = {}

# for n in range(1,6): 
if True:
    for i in range(1,num_folds+1): # for each fold      
        print("processing fold {} out of {}".format(i, num_folds))
        
        #
        # COLLECT TRAINING DATA
        #
        words_list = {}  # elements of type ({'feature_name':feature_value}:class_label)
        j = 1.0    
        k = 0
        speech_samples = []
        speech_correct= []
        for eid in episodes:
            if j > max_eps: break
            if is_training_data(j, i):
                prepare_training(eid, words_list, source)
            j += 1.0
        
        #
        # TRAIN
        #
        word_classifiers = {}
        for word in words_list:
            word_classifiers[word] = train(words_list[word]) 
        
        #check_select_words(word_classifiers)
        
        #
        # EVALUATE
        #  
        j = 1.0
        for eid in episodes:
            #collect triaining data for the fold
            if j > max_eps: break
            if not is_training_data(j, i):
                current_rank = perform_eval(eid, word_classifiers, source)
                #update metrics
                avg_rank += current_rank
                if current_rank == 1: cor_tot = (cor_tot[0]+1,cor_tot[1])
                cor_tot = (cor_tot[0],cor_tot[1]+1)
                #incremental_metrics()
            j += 1.0
    
        print ("acc", cor_tot, cor_tot[0]/cor_tot[1])
        print ("avg rank", avg_rank / cor_tot[1])

