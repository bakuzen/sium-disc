import xml.etree.ElementTree as ET
import sqlite3
import random
import time
import math
import numpy
import nltk
from sklearn import datasets, linear_model
from nltk.stem.snowball import GermanStemmer


#### The following cells are for querying the database (take.db should be in the same folder as this notebook) 


def startdb():
    return sqlite3.connect('take.db')
    
def closedb(conn):
    conn.commit()
    conn.close()


def get_deixis_intercepts(eid):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from deixis_intercepts where episode_id = ?',(eid,))
    result = [row for row in result] # copy
    closedb(conn)
    return result


def get_gaze_intercepts(eid):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from gaze where episode_id = ?',(eid,))
    result = [row for row in result] # copy
    closedb(conn)
    return result

def get_deixis(eid):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from deixis where episode_id = ?',(eid,))
    result = [row for row in result] # copy
    closedb(conn)
    return result


def get_raw_data(eid):
    conn = startdb()
    c = conn.cursor()
    result = c.execute('select * from cv_piece_raw where episode_id = ?',(eid,))
    result = [row for row in result] # copy
    closedb(conn)
    return result


def get_selected_piece(eid):
    conn = startdb()
    c = conn.cursor()
    c.execute('select object from sem where episode_id = ?', (eid,))
    result = c.fetchone()[0]
    closedb(conn)
    return result


#### For extracting features from the database rows

def get_features(sample):
    '''
    A sample consists of raw information retrieved from the database including RGB, HSV, orientation values, as well
    as x,y coordiantes of the object's calcuated centroid, and how it has been skewed.
    
    '''
    features = {}
    f_name = 'f{}'
    f_ind = 1
    for feature in sample[2:-6]: # rgb, hsv, orientation
        features[f_name.format(str(f_ind))] = feature
        f_ind +=1 
    for feature in sample[-2:]: # x, y
        features[f_name.format(str(f_ind))] = feature
        f_ind +=1 
    features['num_edges'] = sample[11]
    if sample[9] == 'left-skewed': features['h_ls'] = 1.0
    else: features['h_ls'] = 0.0
    if sample[9] == 'right-skewed': features['h_rs'] = 1.0
    else: features['h_rs'] = 0.0
    if sample[9] == 'symmetric': features['h_s'] = 1.0
    else: features['h_s'] = 0.0   
        
    if sample[10] == 'top-skewed': features['v_ts'] = 1.0
    else: features['v_ts'] = 0.0
    if sample[10] == 'bottom-skewed': features['v_bs'] = 1.0
    else: features['v_bs'] = 0.0
    if sample[10] == 'symmetric': features['v_s'] = 1.0
    else: features['v_s'] = 0.0           
    return features


##### The follwing cells are for training and evaluation

def train(training_data):
    '''
    The training data is a list of dictionaries and class labels (True or False). The dictionary values are the raw values
    the features for the logreg classifier.
    '''
    x = []
    y = []
    for row in training_data:
        x.append(row[0].values())
        y.append(row[1])
        
    regr = linear_model.LogisticRegression()
    regr.fit(x, y)
    
    return regr


def is_training_data(j, i):
    '''
    returns True if an episode is within a fold's training set
    '''
    return (j < (i-1) * fold_size) or (j > i * fold_size)



# for training data, get the raw features from one sample and max_negs number of negative samples (5 works well)

def process_words(words, eid, utt, max_negs=5):
    '''
    This processes the words for TRAINING. It needs the words dictionary, the episode id, the utterance, and the number of 
    (randomly chosen) negative samples to use for training.
    '''
    selected = get_selected_piece(eid)
    raw_data = get_raw_data(eid)
    random.shuffle(raw_data)
    #grab the positive and negative samples from the set of 15 objects
    neg_samples = []
    pos_sample = None
    for r in raw_data:
        if r[1] == selected: pos_sample = r
        elif len(neg_samples) < max_negs: neg_samples.append(r)
        if len(neg_samples) == max_negs and pos_sample is not None: break
    if pos_sample is None: return
    pos_features = get_features(pos_sample)      
    for word in utt.strip().split():
        #word = stem.stem(word)
        if word not in words:
            words[word] = []
        words[word].append((pos_features,True))
        for neg_sample in neg_samples:
            neg_features = get_features(neg_sample)
            words[word].append((neg_features,False))    


def process_eval_words(objects, eid):
    '''
    the process_words's counterpart for evaluation; this gathers the features for each object
    '''
    raw_data = get_raw_data(eid)
    for row in raw_data:
        objects[row[1]] = get_features(row)      
    return objects

def argmax(dist):
    '''
    find the argmax of a distribution in a dictionary
    '''
    m = ('',0)
    for o in dist:
        if dist[o] > m[1]:
            m = (o,dist[o])
    return m[0]


def calc_entropy(objects):
    entropy = 0.0
    for obj in objects:
        entropy += objects[obj] * math.log(objects[obj])
    return -entropy


def normalise(prob_dists):
    '''
    Given a dictionary of object ids and probabilities, it normalises the dictionary and returns it 
    '''
    new_dists = {}
    k = sum(prob_dists.values())
    for obj in prob_dists:
        new_dists[obj] = prob_dists[obj] / k
    return new_dists


def classify(utt, word_classifiers, objects):
    '''
    given an utterance, the word_classifiers, and the set of objects, this steps through each word in the utterance and each
    object and gets a probability distribution for each object given the results from the word classifiers
    '''
    word_object_dists = []
    for word in utt.strip().split():
        #word = stem.stem(word)
        prob_dists = {}
        if word in word_classifiers: #just ignore words we don't know anything about
            for obj in objects:
                prob_dist = word_classifiers[word].predict_proba(objects[obj].values())
                #prob_dist = word_classifiers[word].prob_classify(objects[obj])
                prob_dists[obj] = prob_dist[0][1]
                #prob_dists[obj] = prob_dist.prob(True)
            prob_dists = normalise(prob_dists)
            word_object_dists.append((word,prob_dists)) #only need the True value, the other can be inferred
    return word_object_dists


def euclidean_dist(x,y,objects):
    '''
    find the euclidean distance of all objects in a dictionary (x=f8, y=f9) and a point
    '''
    euclid_dists = []
    for o in objects:
        euclid_dists.append(math.sqrt((float(objects[o]['f8']) - float(x)) ** 2 + (float(objects[o]['f9']) - float(y)) ** 2))
    return euclid_dists


def point_calc(x1,x2,stddev):
    '''
    this will return a probability, given two points and a std dev ... the larger the std dev, the more mass is given when the
    two points are far away
    '''
    return math.exp(-pow((x1-x2),2.0) / (2.0 * math.pow(stddev,2.0))) 


def combine_with_modality(query_func, eid, objects, filt=-1):
    '''
    This takes an episode id, then queries the db for the set of deixis points on the screen and their durations. For each point,
    if calculates the distance between that point and each object centroid, then determines a normalised probability distribution
    based on the distances. Then it awards the duration to each object based on its probability. This is summed for each deixis
    event, and normalised over for the total deixis duration. That resulting distribution is combined with the NLU distribution
    by additive interpolation.
    '''
    deixis = query_func(eid)
    #for o in object_dist: object_dist[o] = 0.1
    if len(deixis) == 0: return object_dist
    if len(deixis[0]) <= 1: return object_dist
    if 'T' in deixis[0][1] or 'B' in deixis[0][1]: return object_dist
    deixis = deixis[0][1].strip().split()

    total = 0.0
    all_object_probs = {}
    # step through each point
    for point in deixis:
        if ',' not in point: continue #this shouldn't happen, of course
        x,y,d = point.split(',')
        
        if str(d).lower() == 'nan': d = 1.0
        
        # calculate the std dev
        std = numpy.std(euclidean_dist(x, y, objects)) 
        if std == 0.0: std = 0.25
        
        object_probs = {}
        #determine the probabiliy for each object based on a normal distribution from the deixis point
        for obj in objects:
            if obj not in object_probs: object_probs[obj] = 0.0
            ox,oy = objects[obj]['f8'],objects[obj]['f9']
            prob = point_calc(float(ox),float(x),std) * point_calc(float(oy),float(y),std)
            object_probs[obj] = prob 
        object_probs = normalise(object_probs)
        
        
        
        # for the current point, award the duration to each object based on their probabilities
        for obj in object_probs:
            if obj not in all_object_probs: all_object_probs[obj] = 0.0
            all_object_probs[obj] += object_probs[obj] * float(d) 
        total += float(d)
        
    for o in all_object_probs:
        all_object_probs[o] /= total
   
        
    # combine with the NLU distribution
    #for o in all_object_probs:
    #    object_dist[o] = object_dist[o] * alpha +  (1.0-alpha) * (all_object_probs[o] / total)
    #return normalise(object_dist)
    return normalise(all_object_probs)


##### Run the cross validation

# In[940]:

def interpolate(dists, weights):
    
    new_dist = {}
    
    for obj in dists[0]:
        row = 0.0
        i = 0
        for dist in dists:
            row += dist[obj] * weights[i]
            i += 1
        new_dist[obj] = row
            
    return new_dist

'''
     Program begin
'''

conn = startdb()
c = conn.cursor()
res = c.execute('select * from utterance order by episode_id')
#episodes, utterances = zip(*res)
eid_utts = [row for row in res]
closedb(conn)


'''
conn = startdb()
c = conn.cursor()
res = c.execute('select * from asr order by episode_id')
#episodes, utterances = zip(*res)
eid_utts = [row for row in res]
closedb(conn)
'''

stem = GermanStemmer()

fold_size = 100
num_folds = 10
max_eps = 1000
cor_tot = (0.0,0.0)
entropy = 0.0

corr_list = []
all_object_dists = {}
all_objects = {}

#for n in range(1,11):
if True:
    for i in range(1,num_folds+1):
    #for i in range(2,3):            
        print "processing fold {} out of {}".format(i, num_folds)
        time.sleep(1)
    
        #collect triaining data
        words = {}  # elements of type ({'feature_name':feature_value}:class_label)
        j = 1.0    
        for eid, utt in eid_utts:
            if j > max_eps: break
            if is_training_data(j, i):
                process_words(words, eid, utt)
            j += 1.0
        
        #train
        word_classifiers = {}
        for word in words:
            word_classifiers[word] = train(words[word]) 
            
        #evaluate    
        j = 1.0
        for eid, utt in eid_utts:
            #collect triaining data for the fold
            if j > max_eps: break
            if not is_training_data(j, i):
                selected = get_selected_piece(eid)
                objects = {}
                process_eval_words(objects, eid)
                word_object_dists = classify(utt, word_classifiers, objects)
                if len(word_object_dists) == 0: continue
                object_dist = word_object_dists[0][1]
                for w in word_object_dists[1:]:
                    for obj in object_dist:
                        object_dist[obj] = object_dist[obj] + w[1][obj]
                object_dist = normalise(object_dist)
                #deixis_dist = combine_with_modality(get_deixis_intercepts, eid, objects)
                gaze_dist = combine_with_modality(get_gaze_intercepts, eid, objects)
            
                #all_object_dists[eid] = [object_dist, deixis_dist, gaze_dist]
                #all_objects[eid] = objects
                
                #object_dist = interpolate([object_dist, deixis_dist, gaze_dist], [0.72, 0.16, 0.12])
                #object_dist = interpolate([object_dist, deixis_dist, gaze_dist], [0.53, 0.23, 0.24])
                object_dist = interpolate([object_dist, gaze_dist], [0.78, 0.22])
    
                guess = argmax(object_dist)
                entropy += calc_entropy(object_dist)
                if guess == selected:
                    cor_tot = (cor_tot[0] + 1.0, cor_tot[1])
                    corr_list.append(eid)
                cor_tot = (cor_tot[0], cor_tot[1]+1.0)
            j += 1.0
    
    print "acc", cor_tot, cor_tot[0]/cor_tot[1]
    print "entropy:", entropy / cor_tot[1]
   
# Using the discriminative model with raw CV output.
# 
# results
# HandTransc + CV 0.63260654113
# HandTrans + CV + deixis + gaze 0.700297324083
# ASR + CV 0.267690782953
# ASR + CV + deixis + gaze 0.437859266601 
# 
# entropy
# HandTransc + CV  2.57532732956
# HandTrans + CV + deixis + gaze 2.57627877614
# ASR + CV 2.64017106713
# ASR + CV + deixis + gaze 2.6101986041

def find_weights():
    weight = 0.1
    max_cor = -999.0
    max_weight = weight
    while weight < 1.0:
        weight2 = 0.1
        while weight2 < 1.0:
            if weight + weight2 >= 1.0: 
                break
                
            cor_tot = 0.0
            for eid in all_object_dists:
                selected = get_selected_piece(eid)
                weights = [(1-(weight+weight2)), weight, weight2]
                object_dist = interpolate(all_object_dists[eid], weights)
                guess = argmax(object_dist)
                if guess == selected:
                    cor_tot += 1.0  
                
            if cor_tot > max_cor:
                max_cor = cor_tot
                max_weight = weights
                print cor_tot, str(weights)
            weight2 += 0.01
     
        weight += 0.01
    
    max_weight

# hand [0.72, 0.16, 0.12]
# asr [0.53, 0.23, 0.24]





