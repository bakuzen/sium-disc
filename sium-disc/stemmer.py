'''
Created on Oct 22, 2014

@author: casey
'''
from nltk.stem.snowball import GermanStemmer


stem = GermanStemmer()

print stem.stem("rot")
print stem.stem("rote")
print stem.stem("roten")
print stem.stem("rotes")
print stem.stem("roter")